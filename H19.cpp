#include <iostream>

class animal
{
private:
	std::string v;
public:
	animal() {}
	animal(std::string _v) : v(_v) {}
	virtual ~animal() {}

	virtual void voice()
	{
		std::cout << v << '\n';
	}
};

class dog : public animal
{
private:
	std::string d = "Woof";
public:
	void voice() override
	{
		std::cout << d << '\n';
	}
};

class cat : public animal
{
private:
	std::string c = "Mea";
public:
	void voice() override
	{
		std::cout << c << '\n';
	}
};

class mouse: public animal
{
private:
	std::string m = "Pi";
public:
	void voice() override
	{
		std::cout << m << '\n';
	}
};

int main()
{
	dog* p = new dog();
	cat* c = new cat();
	mouse* f = new mouse();
	animal* animals[] = {p, c, f};
	
	for (int i=0; i<3; i++)
	{
		animals[i]->voice();
		delete animals[i];
	}
}
